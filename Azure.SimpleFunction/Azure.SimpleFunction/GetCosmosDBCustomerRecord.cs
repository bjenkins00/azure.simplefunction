
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos;
using CosmoDbConsole.Models;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Linq;

namespace Azure.SimpleFunction
{
    public static class GetCosmosDBCustomerRecord
    {
        [FunctionName("GetCosmosDBCustomerRecord")]
        public static IActionResult Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            string Id = req.Query["Id"];

            string requestBody = new StreamReader(req.Body).ReadToEnd();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            Id = Id ?? data?.Id;

            string responseMessage = "";

            if (string.IsNullOrEmpty(Id))
            {
                responseMessage = "Invalid Id";
            }
            else
            {
                var customer = new Customer();

                try
                {
                    //Create a new instance of the Cosmos Client
                    using (var client = new CosmosClient("AccountEndpoint=https://mindcept-cosmo-db.documents.azure.com:443/;AccountKey=wAx5OioHkV4fjDgQem3MngUlpt4lSVprLtmcS9DcmLAvz6NcAlAv4jmynTWz1oZBAWUSVuudPHBmHTIMHodAIA==;"))
                    {
                        var DB = client.GetDatabase("ChampionProductCosmoDB");
                        var table = client.GetContainer(DB.Id, "Customer");


                        //Get records with 'Smith' as lastname
                        var sqlString = "SELECT * FROM c where c.customerId = " + Id;
                        var query = new QueryDefinition(sqlString);
                        var iterator = table.GetItemQueryIterator<Customer>(sqlString);

                        var queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

                        customer = queryResult.FirstOrDefault();

                        responseMessage = JsonConvert.SerializeObject(customer);

                    };
                }
                catch (System.Exception e)
                {

                    throw;
                }
            }


            return new OkObjectResult(responseMessage);
        }
    }
}
